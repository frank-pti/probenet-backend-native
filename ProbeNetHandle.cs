/*
 * A native ProbeNet Backend module in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Internationalization;
using ProbeNet.Messages.Interface;
using ProbeNet.Messages.Raw;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProbeNet.Backend.Native
{
    /// <summary>
    /// ProbeNet device handle.
    /// </summary>
    [ResourceIconAttribute("ProbeNet.Backend.Native.icon", "empty.png")]
    [TranslatableCaption(Constants.DeviceDomain, "ProbeNet-enabled device")]
    [SourceTypeIdentifier(Constants.ParserTypeName)]
    [Hidden]
    public class ProbeNetHandle : Handle
    {
        private ProbeNetParser parser;
        private IDictionary<string, DeviceDescription> deviceDescriptions;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Backend.Native.ProbeNetHandle"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="installedLanguages">Installed languages.</param>
        /// <param name="serialNumber">Serial number.</param>
        /// <param name="deviceDescriptions">Device descriptions.</param>
        public ProbeNetHandle(
            I18n i18n, IList<string> installedLanguages, string serialNumber,
            IDictionary<string, DeviceDescription> deviceDescriptions) :
            this(i18n,
            FindDefault(deviceDescriptions, installedLanguages).InstalledLanguages,
            FindDefault(deviceDescriptions, installedLanguages).SerialNumber,
            deviceDescriptions,
            new ProbeNetParser(ExtractUuid(deviceDescriptions)))
        {
        }

        protected ProbeNetHandle(
            I18n i18n, IList<string> installedLanguages, string serialNumber,
            IDictionary<string, DeviceDescription> deviceDescriptions, ProbeNetParser parser) :
            base(i18n, installedLanguages, serialNumber, deviceDescriptions)
        {
            this.parser = parser;
            this.parser.DeviceDescriptionReceived += ParserDeviceDescriptionReceived;
            this.parser.MeasurementDescriptionReceived += ParserMeasurementDescriptionReceived;
            this.parser.SequenceDescriptionReceived += ParserSequenceDescriptionReceived;
            this.deviceDescriptions = deviceDescriptions;
            ResolveSequenceMeasurementReferences(deviceDescriptions);
        }

        protected static IEnumerable<Guid> ExtractUuid(IDictionary<string, DeviceDescription> deviceDescriptions)
        {
            return deviceDescriptions.Select(d => d.Value.Uuid).Distinct();
        }

        private void ParserDeviceDescriptionReceived(Messages.Raw.DeviceDescription deviceDescription, string language)
        {
            if (deviceDescriptions.ContainsKey(language)) {
                bool uuidMatch = deviceDescriptions[language].Uuid.Equals(deviceDescription.Uuid);
                bool modelNumberMatch = deviceDescriptions[language].ModelNumber.Equals(deviceDescription.ModelNumber);
                bool serialNumberMatch = deviceDescriptions[language].SerialNumber.Equals(deviceDescription.SerialNumber);
                if (uuidMatch && modelNumberMatch && serialNumberMatch) {
                    deviceDescriptions[language] = deviceDescription;
                } else {
                    Console.WriteLine("Cannot replace device description:");
                    Console.WriteLine("  UUID match  : {0}", uuidMatch);
                    Console.WriteLine("  Model match : {0}", modelNumberMatch);
                    Console.WriteLine("  Serial match: {0}", serialNumberMatch);
                }
            } else {
                deviceDescriptions.Add(language, deviceDescription);
            }
        }

        private void ParserMeasurementDescriptionReceived(
            Messages.Raw.MeasurementDescription measurementDescription, int index, Guid uuid, string language)
        {
            if (deviceDescriptions.ContainsKey(language)) {
                DeviceDescription deviceDescription = deviceDescriptions[language];
                if (deviceDescription.Uuid.Equals(uuid)) {
                    deviceDescription.Measurements[index] = measurementDescription;
                }
            }
        }

        private void ParserSequenceDescriptionReceived(
            Messages.Raw.SequenceDescription sequenceDescription, int index, Guid uuid, string language)
        {
            if (deviceDescriptions.ContainsKey(language)) {
                DeviceDescription deviceDescription = deviceDescriptions[language];
                if (deviceDescription.Uuid.Equals(uuid)) {
                    deviceDescription.Sequences[index] = sequenceDescription;
                }
            }
        }

        protected static DeviceDescription FindDefault(
            IDictionary<string, DeviceDescription> all, IList<string> installedLanguages)
        {
            string defaultLanguage = installedLanguages[0];
            if (all.ContainsKey(defaultLanguage)) {
                return all[defaultLanguage];
            }
            return all.First().Value;
        }

        private void ResolveSequenceMeasurementReferences(IDictionary<string, DeviceDescription> deviceDescriptions)
        {
            foreach (DeviceDescription deviceDescription in deviceDescriptions.Values) {
                ResolveSequenceMeasurementReferences(deviceDescription);
            }
        }

        private void ResolveSequenceMeasurementReferences(DeviceDescription deviceDescription)
        {
            Dictionary<string, MeasurementDescription> measurements = new Dictionary<string, MeasurementDescription>();
            foreach (MeasurementDescription measurement in deviceDescription.Measurements) {
                measurements.Add(measurement.Id, measurement);
            }
            foreach (SequenceDescription sequence in deviceDescription.Sequences) {
                foreach (SequenceDescriptionMeasurementReference reference in sequence.Measurements) {
                    reference.Measurement = measurements[reference.MeasurementId];
                }
            }
        }

        #region implemented abstract members of ProbeNet.Backend.Handle
        /// <inheritdoc/>
        public override int ExampleDataCount
        {
            get
            {
                return 0;
            }
        }

        /// <inheritdoc/>
        public override byte[] GetExampleData(int index)
        {
            return new byte[0];
        }
        #endregion

        /// <inheritdoc/>
        public override IDeviceDescription Description
        {
            get
            {
                if (deviceDescriptions.ContainsKey(i18n.Language)) {
                    return deviceDescriptions[i18n.Language];
                } else {
                    return deviceDescriptions["en"];
                }
            }
        }

        /// <inheritdoc/>
        public override IParser Parser
        {
            get
            {
                return parser;
            }
        }

        /// <inheritdoc/>
        public override bool RequiresLicense
        {
            get
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override TranslationString Caption
        {
            get
            {
                if (deviceDescriptions.ContainsKey(i18n.Language)) {
                    return i18n.TrObject(Constants.DeviceDomain, "{0}", deviceDescriptions[i18n.Language].Caption);
                } else {
                    return i18n.TrObject(Constants.DeviceDomain, "{0}", deviceDescriptions["en"].Caption);
                }
            }
        }

        /// <summary>
        /// Update the device description from device.
        /// </summary>
        public override void UpdateDeviceDescription()
        {
            parser.UpdateDeviceDescription(InstalledLanguages);
        }
    }
}

