﻿using ProbeNet.Communication;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProbeNet.Backend.Native
{
    public class ProbeNetDigitalParser : ProbeNetParser
    {
        private Queue<byte> queue;
        private bool reading;

        public ProbeNetDigitalParser(IEnumerable<Guid> deviceUuid) :
            base(deviceUuid)
        {
            queue = new Queue<byte>();
            reading = false;
        }

        public override void Parse(byte[] data, int offset, int count)
        {
            for (int i = offset; i < offset + count; i++) {
                byte b = data[i];
                if (b == FrameDefinitions.START_BYTE) {
                    queue.Clear();
                    reading = true;
                }
                if (reading) {
                    queue.Enqueue(b);
                }
                if (b == FrameDefinitions.END_BYTE) {
                    reading = false;
                    byte[] buffer = queue.ToArray();
                    byte[] frame = new byte[buffer.Length];
                    if (!buffer.Contains(FrameDefinitions.PAYLOAD_END_BYTE)) {
                        frame = FrameEncoder.Encode(buffer, 1, buffer.Length - 2);
                    } else {
                        Array.Copy(buffer, frame, buffer.Length);
                    }
                    base.Parse(frame, 0, frame.Length);
                }
            }
        }
    }
}
