/*
 * A native ProbeNet Backend module in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Newtonsoft.Json;
using ProbeNet.Communication;
using ProbeNet.DeviceDiscovery;
using ProbeNet.Messages;
using ProbeNet.Messages.Translatable;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProbeNet.Backend.Native
{
    /// <summary>
    /// ProbeNet parser.
    /// </summary>
    public class ProbeNetParser : Parser
    {
        public delegate void DeviceDescriptionReceivedDelegate(
            ProbeNet.Messages.Raw.DeviceDescription deviceDescription, string language);
        public event DeviceDescriptionReceivedDelegate DeviceDescriptionReceived;

        public delegate void MeasurementDescriptionReceivedDelegate(
            ProbeNet.Messages.Raw.MeasurementDescription measurementDescription, int index, Guid uuid, string language);
        public event MeasurementDescriptionReceivedDelegate MeasurementDescriptionReceived;

        public delegate void SequenceDescriptionReceivedDelegate(
            ProbeNet.Messages.Raw.SequenceDescription sequenceDescription, int index, Guid uuid, string language);
        public event SequenceDescriptionReceivedDelegate SequenceDescriptionReceived;

        private FrameParser frameParser;
        private IEnumerable<Guid> deviceUuid;
        private bool enable;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Backend.Native.ProbeNetParser"/> class.
        /// </summary>
        public ProbeNetParser(IEnumerable<Guid> deviceUuid)
        {
            this.deviceUuid = deviceUuid;
            enable = false;
            frameParser = new FrameParser();
            frameParser.FrameParsedString += HandleFrameParsedString;
            frameParser.FrameError += HandleFrameError;
        }

        private void HandleFrameParsedString(string data)
        {
            Message decodedMessage = MessageDecoder.Decode(data);
            string language = decodedMessage.Header.Language;
            ProbeNetDigitalTag tag = decodedMessage.Header.Tag != null ?
                JsonConvert.DeserializeObject<ProbeNetDigitalTag>(decodedMessage.Header.Tag) : null;
            switch (decodedMessage.Body.Type) {
                case ProbeNet.Enums.MessageType.SequenceSeriesMetadata:
                    if (enable) {
                        NotifySendSequenceSeriesMetadata(decodedMessage.Body as ProbeNet.Messages.Base.SequenceSeriesMetadata);
                    }
                    break;
                case ProbeNet.Enums.MessageType.Sequence:
                    if (enable) {
                        NotifySendSequence(decodedMessage.Body as ProbeNet.Messages.Base.Sequence);
                    }
                    break;
                case Enums.MessageType.DeviceDescription:
                    ProcessDeviceDescription(decodedMessage.Body as ProbeNet.Messages.Raw.DeviceDescription, language);
                    break;
                case Enums.MessageType.MeasurementDescription:
                    NotifyMeasurementDescriptionReceived(
                        decodedMessage.Body as ProbeNet.Messages.Raw.MeasurementDescription,
                        tag.Index,
                        tag.Uuid,
                        language);
                    break;
                case Enums.MessageType.SequenceDescription:
                    NotifySequenceDescriptionReceived(
                        decodedMessage.Body as ProbeNet.Messages.Raw.SequenceDescription,
                        tag.Index,
                        tag.Uuid,
                        language);
                    break;
                default:
                    // ignore all other packets
                    Console.WriteLine("ProbeNet Parser: ignoring packet of type {0}", decodedMessage.Body.Type);
                    Console.WriteLine(decodedMessage);
                    break;
            }
        }

        private void HandleFrameError(string error)
        {
            NotifyParsingError(false, error, error);
        }

        #region implemented abstract members of ProbeNet.Backend.Parser
        /// <inheritdoc/>
        public override void Parse(byte[] data, int offset, int count)
        {
            frameParser.Parse(data, offset, count);
        }

        /// <inheritdoc/>
        public override void Reset()
        {
            frameParser.Reset();
        }
        #endregion

        /// <summary>
        /// Request update device description for all the languages given in the list.
        /// </summary>
        /// <param name="languages">The list of language codes.</param>
        public void UpdateDeviceDescription(IList<string> languages)
        {
            foreach (string language in languages) {
                UpdateDeviceDescription(language);
            }
        }

        /// <summary>
        /// Request update device description the given language.
        /// </summary>
        /// <param name="language">The language code.</param>
        public void UpdateDeviceDescription(string language)
        {
            NotifySendMessageToSource(new DeviceDescriptionRequest(), language);
        }

        private void ProcessDeviceDescription(ProbeNet.Messages.Raw.DeviceDescription deviceDescription, string language)
        {
            enable = deviceUuid.Contains(deviceDescription.Uuid);

            if (enable) {
                int emptyMeasurementDescriptionCount = DeviceObserver.CountEmptyMeasurementDescription(deviceDescription);
                int emptySequenceDescriptionCount = DeviceObserver.CountEmptySequenceDescription(deviceDescription);

                NotifyDeviceDescriptionReceived(deviceDescription, language);
                if (emptyMeasurementDescriptionCount != 0 || emptySequenceDescriptionCount != 0) {
                    if (emptyMeasurementDescriptionCount > 0) {
                        for (int i = 0; i < emptyMeasurementDescriptionCount; i++) {
                            NotifySendMessageToSource(new MeasurementDescriptionRequest(i), language, String.Format("{0}", i));
                        }
                    }
                    if (emptySequenceDescriptionCount > 0) {
                        for (int i = 0; i < emptySequenceDescriptionCount; i++) {
                            NotifySendMessageToSource(new SequenceDescriptionRequest(i), language, String.Format("{0}", i));
                        }
                    }
                }
            } else {
                Console.WriteLine("Processing disabled: '{0}' not found", deviceDescription.Uuid);
            }
        }

        private void NotifySendMessageToSource(MessageBody body, string language)
        {
            NotifySendMessageToSource(body, language, null);
        }

        private void NotifySendMessageToSource(MessageBody body, string language, string tag)
        {
            Message message = new Message(new MessageHeader(language, DateTime.Now, tag), body);
            NotifySendMessageToSource(message);
        }

        private void NotifySendMessageToSource(Message message)
        {
            string messageString = MessageEncoder.EncodeToString(message);
            NotifySendDataToSource(FrameEncoder.Encode(messageString));
        }

        private void NotifyDeviceDescriptionReceived(
            ProbeNet.Messages.Raw.DeviceDescription deviceDescription, string language)
        {
            if (DeviceDescriptionReceived != null) {
                DeviceDescriptionReceived(deviceDescription, language);
            }
        }

        private void NotifyMeasurementDescriptionReceived(
            ProbeNet.Messages.Raw.MeasurementDescription measurementDescription, int index, Guid uuid, string langauge)
        {
            if (MeasurementDescriptionReceived != null) {
                MeasurementDescriptionReceived(measurementDescription, index, uuid, langauge);
            }
        }

        private void NotifySequenceDescriptionReceived(
            ProbeNet.Messages.Raw.SequenceDescription sequenceDescription, int index, Guid uuid, string language)
        {
            if (SequenceDescriptionReceived != null) {
                SequenceDescriptionReceived(sequenceDescription, index, uuid, language);
            }
        }
    }
}

