﻿using Internationalization;
using ProbeNet.Messages.Raw;
using System.Collections.Generic;

namespace ProbeNet.Backend.Native
{
    [ResourceIconAttribute("ProbeNet.Backend.Native.icon", "empty.png")]
    [TranslatableCaption(Constants.DeviceDomain, "ProbeNet-enabled device")]
    [SourceTypeIdentifier(Constants.ParserTypeName)]
    [Hidden]
    public class ProbeNetDigitalHandle : ProbeNetHandle
    {
        public ProbeNetDigitalHandle(
            I18n i18n, IList<string> installedLanguages, string serialNumber,
            IDictionary<string, DeviceDescription> deviceDescriptions) :
            base(i18n,
            FindDefault(deviceDescriptions, installedLanguages).InstalledLanguages,
            FindDefault(deviceDescriptions, installedLanguages).SerialNumber,
            deviceDescriptions,
            new ProbeNetDigitalParser(ExtractUuid(deviceDescriptions)))
        {
        }
    }
}
