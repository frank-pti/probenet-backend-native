/*
 * A native ProbeNet Backend module in C#
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;

namespace ProbeNet.Backend.Native
{
    /// <summary>
    /// ProbeNet device entry point.
    /// </summary>
    public class ProbeNetEntryPoint: EntryPoint
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Backend.Native.ProbeNetEntryPoint"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        public ProbeNetEntryPoint (I18n i18n):
            base(i18n, "ProbeNet.Backend.Native.i18n", Constants.DeviceDomain)
        {
        }
    }
}
